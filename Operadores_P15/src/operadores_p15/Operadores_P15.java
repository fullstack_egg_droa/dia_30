/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package operadores_p15;

/**
 *
 * @author droa
 */
public class Operadores_P15 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // Declaracion:
            byte entero;
            short entero1;
            int i;
            long entero3;
            float flotante2;
            double doble;
            boolean condicional;
            char caracter;
            String cadena;
            

       
            // Inicialización:
        
            entero=1;
            entero1=2;
            i=3;
            entero3=4;
            flotante2 = 1000.1888888889f; // Un flotante no es decimal es entero
            doble=3000.6566;
            condicional=false;
            caracter = '\u0064'; //
            cadena="abc";
            
            // link extensiones variables
            // http://dis.um.es/~lopezquesada/documentos/IES_1314/IAW/curso/UT3/java/java3/tutorial13.html#:~:text=long%3A%20El%20tipo%20de%20dato,el%20m%C3%A1ximo%209%2C223%2C372%2C036%2C854%2C775%2C807%20(inclusive)
            
            
            
            // Operadores
            
            long resultado = entero + entero1 + entero3;
            double resultado_d = (float)entero3 / (float)i;
            i = i + 1;
            i--;
            i++;
            entero = -1;
            condicional = doble > flotante2;
            
            
            //Presentación
            
            System.out.println("El Caracter guardado en la variable char(caracter): "+caracter + ". y El resultado de la suma de diferentes tipos de enteros es: " + resultado);
            System.out.println(resultado);
            System.out.println(flotante2);
            System.out.println(""+condicional);
            System.out.println("Division de Enteros:"+resultado_d);
            
                    
    }
    
}
