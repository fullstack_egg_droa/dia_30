/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package declaracionvariables;

/**
 *
 * @author droa
 */
public class DeclaracionVariables {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // Declaracion:
            byte entero;
            short entero1;
            int entero2;
            long entero3;
            float flotante2;
            double doble;
            boolean condicional;
            char caracter;
            String cadena;
       
            // Inicialización:
        
            entero=1;
            entero1=2;
            entero2=3;
            entero3=4;
            flotante2 = 1.1; // Un flotante no es decimal es entero
            doble=3.6566;
            condicional=false;
            caracter = '\u0061';
            cadena="abc";
                    
        
    }
    
}
